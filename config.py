class Config:
    WEBHOOK_URL = None
    TG_API_KEY = None
    TG_CHAT_ID = None
    AVATAR_URL = 'https://gitlab.com/sdbs_cz/piss-bot/-/raw/master/piss.jpg'
    EVENT_TEMPLATE = "<br>".join((
        "<b>{title}</b>",
        "{date} {location}",
        "{url}",
        "/// /// ///",
        "{description}"
    ))
    SOON_TEMPLATE = "FYI: \"<b>{title}</b>\" is starting {when}!"
